
// Playing Cards Lab
// Eric Johnson

#include <iostream>
#include <conio.h>

using namespace std;

enum Rank
{
	TWO = 2,
	THREE = 3,
	FOUR = 4,
	FIVE = 5,
	SIX = 6,
	SEVEN = 7,
	EIGHT = 8,
	NINE = 9,
	TEN = 10,
	JACK = 11,
	QUEEN = 12,
	KING = 13,
	ACE = 14
};
enum Suit
{
	SPADES, HEARTS, DIAMONDS, CLUBS
};
struct Card
{
	Rank Rank;
	Suit Suit;
};

void PrintCard(Card card)
{
	cout << "The ";
	switch (card.Rank)
	{
	case 2: cout << "TWO"; break;
	case 3: cout << "THREE"; break;
	case 4: cout << "FOUR"; break;
	case 5: cout << "FIVE"; break;
	case 6: cout << "SIX"; break;
	case 7: cout << "SEVEN"; break;
	case 8: cout << "EIGHT"; break;
	case 9: cout << "NINE"; break;
	case 10: cout << "TEN"; break;
	case 11: cout << "JACK"; break;
	case 12: cout << "QEEN"; break;
	case 13: cout << "KING"; break;
	case 14: cout << "ACE"; break;
	default: break;
	}
	cout << " of ";
	switch (card.Suit)
	{
	case 0: cout << "CLUBS "; break;
	case 1: cout << "DIAMONDS "; break;
	case 2: cout << "HEARTS "; break;
	case 3: cout << "SPADES "; break;
	default: break;
	}
	cout << "\n\n";
}

Card HighCard(Card card1, Card card2)
{
	if (card1.Rank > card2.Rank)
	{
		return card1;
	}
	if (card1.Rank < card2.Rank)
	{
		return card2;
	}
	if (card1.Rank == card2.Rank)
	{
		return card1;
	}
}

int main()
{
	Card card1;
	Card card2;
	//Card card3;
	//Card card4;

	Card High1;

	card1.Suit = HEARTS;
	card1.Rank = KING;

	card2.Suit = CLUBS;
	card2.Rank = TEN;
	
	/*card3.Suit = SPADES;
	card3.Rank = FOUR;

	card4.Suit = DIAMONDS;
	card4.Rank = TWO;*/

	PrintCard(card1);
	PrintCard(card2);
	//PrintCard(card3);
	//PrintCard(card4);
	
	High1 = HighCard(card1, card2);
	cout << "\nThe highcard is ";
	PrintCard(High1);

	_getch();
	return 0;
}
